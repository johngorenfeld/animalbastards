/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 *
 */

import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet
} from 'react-native';
import Animals from './src/screens/AnimalsSwipeView';


export default function App() {
  return (
        <Animals/>
  )
};

