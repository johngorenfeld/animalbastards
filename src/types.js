export type Action = String;

export type Animal = {
    name: String,
    reasons: Array<String>,
    impression: boolean,
    judgment: boolean,
    tags: Array<String>
}

export type AnimalBastardsState = {
    currentIndex: number,
    animals: ?Array<Animal>
}

