import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Swiper from 'react-native-deck-swiper';
import { material } from 'react-native-typography';
import { BASTARD, NOT_BASTARD } from '../constants';
import { Animal } from '../types';
import RethinkView from './RethinkView';
import fetchAnimals from '../utils/fetchAnimals';

type Props = {
  actions: {
    requestAnimals: Function
  }
}

type State = {
  animals: Array<Animal>,
  index: number,
  modalIsVisible: boolean
}

export default class AnimalsSwipeView extends Component<Props, State> {
    constructor(props: Props) {
      super(props);

      const animals = fetchAnimals();

      this.state = {
        animals: animals,
        index: 0,
        modalIsVisible: false
      };
    }
  
    _onSwipe(index, isBastard) {
      let newAnimals = this.state.animals;
      newAnimals[index].impression = isBastard;
      // TODO: Stop the swipe
      this.setState({
        modalIsVisible: true
      })
    }

    render() {
      const { animals, index, modalIsVisible } = this.state;

      if (!animals) {
        return (
          <Text>No animals yet!</Text>
        )
      }
      return (
        <View style={styles.container}>
        <RethinkView
          animal={animals[index]}
          isVisible={modalIsVisible}
        />
        <Swiper
            cards={animals}
            renderCard={(animal) => {
                return (
                    <View style={styles.card}>
                    {animal.imageURL &&
                     <Image
                      source={{uri: animal.imageURL}}
                      style={{width: 250, height: 300}}
                     />
                    }
                    <Text style={material.headline}>{animal.name}</Text>
                    </View>
                )
            }}
            onSwipedLeft={(cardIndex) => this._onSwipe(cardIndex, BASTARD)}
            onSwipedRight={(cardIndex) => this._onSwipe(cardIndex, NOT_BASTARD)}
            onSwipedAll={() => {console.log('onSwipedAll')}}
            cardIndex={0}
            backgroundColor={'#22cc44'}>
        </Swiper>
      </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#F5FCFF'
    },
    card: {
      flex: 1,
      borderRadius: 4,
      borderWidth: 2,
      borderColor: '#E8E8E8',
      justifyContent: 'center',
      backgroundColor: 'white',
      padding: 15
    },
    text: {
      textAlign: 'center',
      fontSize: 50,
      backgroundColor: 'transparent'
    },
    done: {
      textAlign: 'center',
      fontSize: 30,
      color: 'white',
      backgroundColor: 'transparent'
    }
  })