import React, { Component } from 'react';
import {
    Dimensions,
    StyleSheet,
    Text,
    View
} from 'react-native';
import { BASTARD, NOT_BASTARD } from '../constants';
import { Animal } from '../types';

type Props = {
    animal: Animal,
    isVisible: boolean
}

const {height, width} = Dimensions.get('window');
const EmptyView = ( <View><Text>Empty view</Text></View> );


export default class RethinkView extends Component<Props> {

    getReason(impression: boolean) {
        const { animal } = this.props;
        const reasonNum = impression === BASTARD ? 0 : 1; // Reason is opposite of what user thinks
        return animal.reasons[reasonNum];
    }

    render() {
        const { animal } = this.props;
        if (!animal) {
            return EmptyView;
        }
        const { impression, reasons } = animal;
        if (!impression) {
            return EmptyView;
        }

        const reason = this.getReason(impression);
  
        const descriptionText = impression === BASTARD ? 'a bastard' : 'not a bastard';
        const button1text = impression === BASTARD ? 'Bastard' : 'Not a bastard';
        const button2text = impression === BASTARD ? "Still don\'t think it's a bastard" : 'Still a bastard, though';
  
        return (
          <View style={styles.container}>
            <Text>Are you sure it's {descriptionText}?</Text>
            <Text>{reason}</Text>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#000',
        // height: height,
        // left: 0,
        // position: 'absolute',
        // top: 0,
        // width: window,
        // zIndex: 9999
    }

});