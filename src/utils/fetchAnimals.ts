// TODO: ImageMap could become part of State in order to make this more efficient
// but P.O.I.T.T.R.O.A.E. 


export default function fetchAnimals() {
    const json = require('../assets/animals.json');
    const { posts } = json;

    const animals = posts.map( ( animal ) => {
        const { id, title, content, tags, attachments } = animal;

        let imageURL = null;
        let thumbnailURL = null;
        
        if (attachments) {
            const attachment = attachments[0];
            if (attachment) {
                const { images } = attachment;
                if (images) {
                    const { full, thumbnail} = images;
                    if (full) {
                        imageURL = full.url;
                    }
                    if (thumbnail) {
                        thumbnailURL = thumbnail.url;
                    }
                }
            }
        }

        const { custom_fields } = animal
        const { bastard_reason, not_bastard_reason } = custom_fields;
        const reasons = [ bastard_reason, not_bastard_reason ];

        return {
            id: id,
            name: title,
            content: content,
            tags: tags,
            imageURL: imageURL,
            thumbnail: thumbnailURL,
            reasons: reasons
        }
    });
    return animals;
}  